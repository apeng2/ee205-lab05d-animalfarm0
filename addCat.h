/////////////////////////////////////////////////////////////////////////////
//////
/////// University of Hawaii, College of Engineering
/////// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///////
/////// @file addCats.h
/////// @version 1.0
///////
/////// @author Adrian Peng <apeng2@hawaii.edu>
/////// @date 15_Feb_2022
///////
/////////////////////////////////////////////////////////////////////////////////
#pragma once

#include "catDatabase.h"
#include "main.h"



int addCat(char *addName, char addGender, char addBreed, bool addIsFixed, float addWeight);




