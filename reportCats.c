/////////////////////////////////////////////////////////////////////////////
////
///// University of Hawaii, College of Engineering
///// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
/////
///// @file reportCats.c
///// @version 1.0
/////
///// @author Adrian Peng <apeng2@hawaii.edu>
///// @date 15_Feb_2022
/////
///////////////////////////////////////////////////////////////////////////////
#include "reportCats.h"

int printCat( int index ) {

      if (index < 0 || index > index_num) {
               printf("animalFarm0: Bad cat [%d]", index);
                     return 0;
                        }

         else {
                  printf("Cat index = [%d]   Name: [%s]   Gender: [%d]   Breed: [%d]   isFixed: [%d]   Weight: [%.1f]\n", index, indexing[index].name, indexing[index].gender, indexing[index].breed, indexing[index].isFixed, indexing[index].weight);
                     }
            return 0;
}



int printAllCats() {

      for (int index = 0; index < index_num; index = index + 1) {
               printf("Cat index = [%d]   Name: [%s]   Gender: [%d]   Breed: [%d]   isFixed: [%d]   Weight: [%.1f]\n", index, indexing[index].name, indexing[index].gender, indexing[index].breed, indexing[index].isFixed, indexing[index].weight);
                  }
         return 0;
}








