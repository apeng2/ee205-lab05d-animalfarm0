###############################################################################
### University of Hawaii, College of Engineering
### @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
###
### @file Makefile
### @version 1.0 - Initial version
###
### Build and test database program
###
### @author Adrian Peng <apeng2@hawaii.edu>
### @date 20_Feb_2022
###
### @see https://www.gnu.org/software/make/manual/make.html
###############################################################################

TARGET = animalFarm



all: $(TARGET)



CC = gcc
CFLAGS = -Wall -Wextra $(DEBUG_FLAGS)



debug: DEBUG_FLAGS = -g -DDEBUG
debug: clean $(TARGET)





addCat.o: addCat.c addCat.h
	$(CC) $(CFLAGS) -c addCat.c

reportCats.o: reportCats.c reportCats.h
	$(CC) $(CFLAGS) -c reportCats.c

updateCats.o: updateCats.c updateCats.h
	$(CC) $(CFLAGS) -c updateCats.c

deleteCats.o: deleteCats.c deleteCats.h
	$(CC) $(CFLAGS) -c deleteCats.c





animalFarm.o: main.c catDatabase.h addCat.h reportCats.h updateCats.h deleteCats.h
	$(CC) $(CFLAGS) -c main.c

animalFarm: animalFarm.o addCat.o reportCats.o updateCats.o deleteCats.o
	$(CC) $(CFLAGS) -o $(TARGET) animalFarm.o addCat.o reportCats.o updateCats.o deleteCats.o



test: $(TARGET)
	./$(TARGET)

clean:
	rm -f $(TARGET) *.o

