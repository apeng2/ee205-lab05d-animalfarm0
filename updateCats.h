/////////////////////////////////////////////////////////////////////////////
////////
///////// University of Hawaii, College of Engineering
///////// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
/////////
///////// @file updateCats.h
///////// @version 1.0
/////////
///////// @author Adrian Peng <apeng2@hawaii.edu>
///////// @date 20_Feb_2022
/////////
///////////////////////////////////////////////////////////////////////////////////
#pragma once
#include "main.h"


int updateCatName( int index, char *newName);

int updateCatWeight( int index, float newWeight);

int updateCatWeight( int index, float newWeight);




