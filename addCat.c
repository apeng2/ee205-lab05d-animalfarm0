/////////////////////////////////////////////////////////////////////////////
////
///// University of Hawaii, College of Engineering
///// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
/////
///// @file addCats.c
///// @version 1.0
/////
///// @author Adrian Peng <apeng2@hawaii.edu>
///// @date 15_Feb_2022
/////
///////////////////////////////////////////////////////////////////////////////
#include "addCat.h"


int addCat(char *addName, char addGender, char addBreed, bool addIsFixed, float addWeight) {

   if (index_num >= max_cats) {
      printf("Error: Database Full. %s was not added to database\n", addName);
      return 0;
   }
   if (strlen(addName) > max_characters) {
      printf("Error: %s's name is too Long\n", addName);
      return 0;
   }
   if (strlen(addName) == 0) {
      printf("Error: Name can't be blank\n");
      return 0;
      }
   if (addWeight <= 0) {
      printf("Error: %s's weight must be > 0\n", addName);
      return 0;
      }

   else {
      strcpy( indexing[index_num].name, addName);
      indexing[index_num].gender = addGender;
      indexing[index_num].breed = addBreed;
      indexing[index_num].isFixed = addIsFixed;
      indexing[index_num].weight = addWeight;

      printf("%s's index number is: %d\n",addName, index_num);
         index_num = index_num + 1;
            }
      return 0;
}



