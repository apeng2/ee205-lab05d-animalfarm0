#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#define max_cats 20
#define max_characters 30
// catDatabase
enum Gender {UNKNOWN_GENDER, MALE, FEMALE};
enum Breed  {UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};


struct cat_data {
   char        name[max_cats];
   enum Gender gender;
   enum Breed  breed;
   bool        isFixed;
   float       weight;

}indexing[max_characters];


// addCat
int index_num = 0;
int addCat(char *addName, char addGender, char addBreed, bool addIsFixed, float addWeight) {

   if (index_num >= max_cats) {
      printf("Error: Database Full. %s was not added to database\n", addName);
      return 0;
   }
   if (strlen(addName) > max_characters) {
      printf("Error: %s's name is too Long\n", addName);
      return 0;
   }
   if (strlen(addName) == 0) {
      printf("Error: Name can't be blank\n");
      return 0;
   }
   if (addWeight <= 0) {
      printf("Error: %s's weight must be > 0\n", addName);
      return 0;
   }
  
   else {
   strcpy( indexing[index_num].name, addName);
   indexing[index_num].gender = addGender;
   indexing[index_num].breed = addBreed;
   indexing[index_num].isFixed = addIsFixed;
   indexing[index_num].weight = addWeight;
    
   printf("%s's index number is: %d\n",addName, index_num);
   index_num = index_num + 1;
   }
   return 0;
}

// reportCats

int printCat( int index ) {
   
   if (index < 0 || index > index_num) {
      printf("animalFarm0: Bad cat [%d]", index);
      return 0;
   }
   
   else {
      printf("Cat index = [%d]   Name: [%s]   Gender: [%d]   Breed: [%d]   isFixed: [%d]   Weight: [%.1f]\n", index, indexing[index].name, indexing[index].gender, indexing[index].breed, indexing[index].isFixed, indexing[index].weight);
   }
   return 0;
}



int printAllCats() {

   for (int index = 0; index < index_num; index = index + 1) {
      printf("Cat index = [%d]   Name: [%s]   Gender: [%d]   Breed: [%d]   isFixed: [%d]   Weight: [%.1f]\n", index, indexing[index].name, indexing[index].gender, indexing[index].breed, indexing[index].isFixed, indexing[index].weight);
   }
   return 0;
}



// updateCats

int updateCatName( int index, char *newName) {
   strcpy(indexing[index].name, newName);
   printf("New name is now: %s\n", indexing[index].name);
   return 0;
}

int updateCatWeight( int index, float newWeight) {
   if (newWeight < 0) {
      printf("Error: Weight must be > 0");
      return 0;
   }

   indexing[index_num].weight = newWeight;
   printf("New weight is now: %.1f\n", indexing[index].weight);
   return 0;
}

int fixCat ( int index ) {
   indexing[index].isFixed = 1;
   return 0;

}


// deleteCats

int deleteAllCats() {
   index_num = 0;
   return 0;
}


int main() {
   
   addCat( "Loki", MALE, PERSIAN, true, 8.5 ) ;
   addCat( "Milo", MALE, MANX, true, 7.0 ) ;
   addCat( "Bella", FEMALE, MAINE_COON, true, 18.2 ) ;
   addCat( "Kali", FEMALE, SHORTHAIR, false, 9.2 ) ;
   addCat( "Trin", FEMALE, MANX, true, 12.2 ) ;
   addCat( "Chili", UNKNOWN_GENDER, SHORTHAIR, false, 19.0 ) ;

   printAllCats();

   printCat(3);

   updateCatName( 3, "Ed");
   
   printCat(3);

   fixCat( 3 );

   printCat(3);

   return 0;
  
   }









