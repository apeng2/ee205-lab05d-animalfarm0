///////////////////////////////////////////////////////////////////////////
/////
///// University of Hawaii, College of Engineering
///// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
/////
///// @file main.h
///// @version 1.0
/////
///// @author Adrian Peng <apeng2@hawaii.edu>
///// @date 15_Feb_2022
/////
///////////////////////////////////////////////////////////////////////////////
#pragma once

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#define max_cats 20
#define max_characters 30

int index_num = 0



