/////////////////////////////////////////////////////////////////////////////
//////
/////// University of Hawaii, College of Engineering
/////// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///////
/////// @file catDatabase.h
/////// @version 1.0
///////
/////// @author Adrian Peng <apeng2@hawaii.edu>
/////// @date 20_Feb_2022
///////
/////////////////////////////////////////////////////////////////////////////////
#include "main.h"

enum Gender {UNKNOWN_GENDER, MALE, FEMALE};
enum Breed  {UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};


struct cat_data {
   char        name[max_characters];
   enum Gender gender;
   enum Breed  breed;
   bool        isFixed;
   float       weight;

}indexing[max_cats];


