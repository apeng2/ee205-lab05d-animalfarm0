///////////////////////////////////////////////////////////////////////////
///
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file main.c
/// @version 1.0
///
/// @author Adrian Peng <apeng2@hawaii.edu>
/// @date 15_Feb_2022
///
/////////////////////////////////////////////////////////////////////////////

#include "main.h"
#include "catDatabase.h"
#include "addCat.h"
#include "reportCats.h"
#include "updateCats.h"
#include "deleteCats.h"



int main() {

addCat( "Loki", MALE, PERSIAN, true, 8.5 ) ;
   addCat( "Milo", MALE, MANX, true, 7.0 ) ;
   addCat( "Bella", FEMALE, MAINE_COON, true, 18.2 ) ;
   addCat( "Kali", FEMALE, SHORTHAIR, false, 9.2 ) ;
   addCat( "Trin", FEMALE, MANX, true, 12.2 ) ;
   addCat( "Chili", UNKNOWN_GENDER, SHORTHAIR, false, 19.0 ) ;

   printAllCats();

   printCat(3);

   updateCatName( 3, "Ed");

   printCat(3);
   
   fixCat( 3 );

   printCat(3);

   return 0;
}





